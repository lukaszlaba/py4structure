*************************************************************************
*                                                                       *
*                         Py4Structure 0.1                             *
*        Structural engeenering design python scripts for SeePy         *
*                                                                       *
*                        (c) 2017 Lukasz Laba                           *
*                                                                       *
*************************************************************************

Py4Structure is a tool to create interactive report for scientific calculation 
you made with python script.
The mission of the project is to provide a simple and practical tool that 
will interest engineers to use Python language in their daily work.

Py4Structure is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Py4Structure is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

-------------------------------------------------------------------------
* Visible changes:
-------------------------------------------------------------------------
  SeePy 0.0.1 (pre-alpha)
  - first PyPI version
  SeePy 0.1 (alpha)
  - alpha version
-------------------------------------------------------------------------
* Prerequisites:
-------------------------------------------------------------------------

  Python 2.7.
  Non-standard Python library needed:
  - SeePy
  - Strupy

-------------------------------------------------------------------------
* To install Py4Structure:
-------------------------------------------------------------------------

  After the Python and needed library  was installed:
  On Windows use "pip install Py4Structure" in Python shell.

  To run Py4Structure GUI execute Py4Structure.py from installed Py4Structure
  package folder - probabiliit is "C:\Python27\Lib\site-packages\py4structure"
  For easy run make shortcut on your system pulpit to this file.
  There is install instruction on project website.
  https://seepy.org
  https://bitbucket.org/lukaszlaba/seepy/wiki/installing
  Linux compatible.
   
-------------------------------------------------------------------------
* Other information :
-------------------------------------------------------------------------

  - Project website: 
    http://seepy.org
    http://struthon.org
    https://bitbucket.org/lukaszlaba/py4structure/wiki/Home
  - E-mail : lukaszlab@o2.pl


=========================================================================
