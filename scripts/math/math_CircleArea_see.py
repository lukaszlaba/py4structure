import math

#!##*Circle Area calculation*

#%img math_CircleArea_fig1.png

#! For input data

r = 657 #<< - circle radius

import math

pi = math.pi #! - pi value

#! from formula that everyone know

Area = pi * r**2 #%tex

#! we have

Area #! - circle area

#! ###So, the area of circle diameter %(r)s is %(Area)s .

'''
SeeDescription :
Category - Math

Wylicza pole koła.
'''



